//
//  Tile.h
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import "CCNode.h"

@interface Tile : CCNode

@property (nonatomic, assign) NSInteger value;
@property (nonatomic, assign) BOOL wasMergedThisRound;

- (void) updateValue;

@end
