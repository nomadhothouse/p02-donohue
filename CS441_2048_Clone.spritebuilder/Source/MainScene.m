//
//  MainScene.m
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import "MainScene.h"
#import "Grid.h"

@implementation MainScene {
    Grid *_grid; // this is the grid being used
    CCLabelTTF *_scoreLabel, *_bestLabel; // the current score and the best score
}


// http://cocos2d.spritebuilder.com/docs/api/Classes/CCBReader.html

- (void) didLoadFromCCB {
    [_grid addObserver:self forKeyPath:@"score" options:0 context:NULL];
    
    [[NSUserDefaults standardUserDefaults]addObserver:self
                                           forKeyPath:@"best"
                                              options:0
                                              context:NULL];
    // load highscore
    [self updateBest];
}


// https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/KeyValueObserving/Articles/KVOBasics.html

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary *)change
                        context:(void *)context {
    if ([keyPath isEqualToString:@"score"]) {
        _scoreLabel.string = [NSString stringWithFormat:@"%ld", (long)_grid.score];
    } else if ([keyPath isEqualToString:@"best"]) {
        [self updateBest];
    }
}

// http://stackoverflow.com/questions/3262362/what-is-dealloc-in-objective-c

- (void)dealloc {
    [_grid removeObserver:self forKeyPath:@"score"];
}

// HANDLERS

- (void) upButton {
    [_grid moveUp];
}

- (void) rightButton {
    [_grid moveRight];
}

- (void) downButton {
    [_grid moveDown];
}

- (void) leftButton {
    [_grid moveLeft];
}

- (void) updateBest {
    // new best score
    // this uses user's default database
    // https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSUserDefaults_Class/
    NSNumber *newBest = [[NSUserDefaults standardUserDefaults]objectForKey:@"best"];
    if (newBest) {
        _bestLabel.string =[NSString stringWithFormat:@"%d", [newBest intValue]];
    }
}

@end