//
//  GameEnd.h
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/5/16.
//

#import "CCNode.h"

@interface GameEnd : CCNode

- (void) setMsg:(NSString *) msg score:(NSInteger)score;

@end
