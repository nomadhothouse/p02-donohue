//
//  Tile.m
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import "Tile.h"

@implementation Tile {
    // the number value of the tile
    CCLabelTTF *_valueLabel;
    // the colored square background of the tile
    CCNodeColor *_backgroundNode;
}

// http://stackoverflow.com/questions/4165872/correct-syntax-for-objective-c-init-method

- (id) init {
    self = [super init];

    if (self) { self.value = (arc4random() % 2 + 1) * 2; }
    
    return self;
}

// http://cocos2d.spritebuilder.com/docs/api/Classes/CCBReader.html

- (void) didLoadFromCCB {
    [self updateValue];
}

- (void) updateValue {
    _valueLabel.string = [NSString stringWithFormat:@"%ld", (long)self.value];
    
    CCColor *backgroundColor = nil;
    CCColor *fontColor = nil;
    
    // font colors for tiles
    if(self.value <= 4){
        fontColor = [CCColor colorWithRed:95.f/255.f green:84.f/255.f blue:71.f/255.f];
    }else{
        fontColor = [CCColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f];
    }
    
    // all the colors for each tile
    switch (self.value) {
        case 2:
            backgroundColor = [CCColor colorWithRed:234.f/255.f green:222.f/255.f blue:209.f/255.f];
            break;
        case 4:
            backgroundColor = [CCColor colorWithRed:231.f/255.f green:218.f/255.f blue:189.f/255.f];
            break;
        case 8:
            backgroundColor = [CCColor colorWithRed:239.f/255.f green:162.f/255.f blue:98.f/255.f];
            break;
        case 16:
            backgroundColor = [CCColor colorWithRed:243.f/255.f green:129.f/255.f blue:76.f/255.f];
            break;
        case 32:
            backgroundColor = [CCColor colorWithRed:244.f/255.f green:101.f/255.f blue:72.f/255.f];
            break;
        case 64:
            backgroundColor = [CCColor colorWithRed:244.f/255.f green:68.f/255.f blue:38.f/255.f];
            break;
        case 128:
            backgroundColor = [CCColor colorWithRed:233.f/255.f green:198.f/255.f blue:87.f/255.f];
            break;
        case 256:
            backgroundColor = [CCColor colorWithRed:233.f/255.f green:196.f/255.f blue:73.f/255.f];
            break;
        case 512:
            backgroundColor = [CCColor colorWithRed:222.f/255.f green:183.f/255.f blue:6.f/255.f];
            break;
        case 1024:
            backgroundColor = [CCColor colorWithRed:219.f/255.f green:174.f/255.f blue:0.f/255.f];
            break;
        case 2048:
            backgroundColor = [CCColor colorWithRed:233.f/255.f green:185.f/255.f blue:11.f/255.f];
            break;
        default:
            backgroundColor = [CCColor greenColor];
            break;
    }
    
    _valueLabel.color = fontColor;
    _backgroundNode.color = backgroundColor;
}

@end