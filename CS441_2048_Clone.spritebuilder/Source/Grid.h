//
//  Grid.h
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import "CCNodeColor.h"

@interface Grid : CCNodeColor

@property (nonatomic, assign) NSInteger score;

- (void)moveLeft;
- (void)moveRight;
- (void)moveUp;
- (void)moveDown;

@end
