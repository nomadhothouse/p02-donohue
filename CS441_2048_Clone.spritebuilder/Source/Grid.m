//
//  Grid.m
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import "Grid.h"
#import "Tile.h"
#import "GameEnd.h"

@implementation Grid {
  NSMutableArray *gridArr;
  CGFloat colWidth, colHeight, tileMarginX, tileMarginY;
  NSNull *nullTile;
}

//=====================CONSTANTS=====================

static const NSInteger GRID_SZ = 4;

static const NSInteger NUM_START = 2;

static const NSInteger WIN_VAL = 2048;

//=====================LOADING=====================

// CCB reference
// http://cocos2d.spritebuilder.com/docs/api/Classes/CCBReader.html

- (void)didLoadFromCCB {
  [self drawGrid];

  // empty tile
  nullTile = [NSNull null];

  // init grid
  gridArr = [NSMutableArray array];
  for (int i = 0; i < GRID_SZ; i++) {
    gridArr[i] = [NSMutableArray array];
    for (int j = 0; j < GRID_SZ; j++) {
      gridArr[i][j] = nullTile;
    }
  }
  // spawn start
  [self createStartTiles];
    
  // UISwipeGesture Recognition -
  // http://stackoverflow.com/questions/17052561/add-uiswipegesturerecognition-to-a-custom-view-ios

  // left
  UISwipeGestureRecognizer *swipeL = [[UISwipeGestureRecognizer alloc]initWithTarget:self
                                       action: @selector(moveLeft)];
    
  swipeL.direction = UISwipeGestureRecognizerDirectionLeft;
  [[[CCDirector sharedDirector]view]
   addGestureRecognizer: swipeL];
    
  //right
  UISwipeGestureRecognizer *swipeR = [[UISwipeGestureRecognizer alloc]initWithTarget:self
                                       action:@selector(moveRight)];
    
  swipeR.direction = UISwipeGestureRecognizerDirectionRight;
  [[[CCDirector sharedDirector]view]
   addGestureRecognizer:swipeR];
 
  // up
  UISwipeGestureRecognizer *swipeU = [[UISwipeGestureRecognizer alloc]initWithTarget:self
                                       action:@selector(moveUp)];
    
  swipeU.direction = UISwipeGestureRecognizerDirectionUp;
  [[[CCDirector sharedDirector]view]
  addGestureRecognizer:swipeU];
 
  // down
  UISwipeGestureRecognizer *swipeD = [[UISwipeGestureRecognizer alloc]initWithTarget:self
                                       action:@selector(moveDown)];
    
  swipeD.direction = UISwipeGestureRecognizerDirectionDown;
  [[[CCDirector sharedDirector]view]
   addGestureRecognizer:swipeD];
}

//=====================NEW ROUND OF MOVES=====================

- (BOOL) canMove {
  for (int i = 0; i < GRID_SZ; i++) {
    for (int j = 0; j < GRID_SZ; j++) {
      Tile *tile = gridArr[i][j];

      // no tile
      if ([tile isEqual: nullTile]) {
        return YES;
      } else {
        // check neighbors
        NSArray *neighbors = [self getNeighbors: i
                                              y: j];

        for (id nTile in neighbors) {
           if (nTile != nullTile) {
              Tile *neighbor = (Tile *) nTile;
              if (neighbor.value == tile.value) {
                 return YES;
              }
           }
        }
      }
    }
  }

  return NO;
}

- (void) newRound {
   [self createRandTile];
   for (int i = 0; i < GRID_SZ; i++) {
      for (int j = 0; j < GRID_SZ; j++) {
         Tile *tile = gridArr[i][j];
         
         if (![tile isEqual: nullTile]) {
            // reset merged
            tile.wasMergedThisRound = NO;
         }
      }
   }
   if (![self canMove]) {
      [self loseGame];
   }
}

//=====================ENDING THE GAME=====================

- (void) finishGame:(NSString *) msg {
    
  // CCNode reference
  // http://cocos2d.spritebuilder.com/docs/api/Classes/CCNode.html
    
  GameEnd *endModal = (GameEnd *)[CCBReader load:@"GameEnd"];

  // http://www.cocos2d-swift.org/docs/api/Classes/CCNode.html#//api/name/positionType
  [endModal setZOrder: INT_MAX];
  [endModal setPositionType: CCPositionTypeNormalized];
  [endModal setPosition: ccp(0.5, 0.5)];
  [endModal setMsg:msg score:self.score];

  [self addChild:endModal];
  
  // https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSUserDefaults_Class/
  NSNumber *bestScore = [[NSUserDefaults standardUserDefaults]
                         objectForKey:@"best"];
  if (self.score > [bestScore intValue]) {
    // new best
    bestScore = [NSNumber numberWithLong:
                 self.score];
      
    [[NSUserDefaults standardUserDefaults]setObject:bestScore
                                             forKey:@"best"];
      
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}

- (void) winGame {
  [self finishGame:@"You win!"];
}

- (void)loseGame {
  [self finishGame:@"You lose."];
}

//=====================HANDLING THE INPUT=====================

- (void)moveLeft {
  // move left in x direction
  [self move:
   ccp(-1, 0)
   ];
}

- (void)moveRight {
  // move right in x direction
  [self move:
   ccp(1, 0)
   ];
}

- (void)moveDown {
   // move down in y direction
  [self move:
   ccp(0, -1)
   ];
}

- (void)moveUp {
   // move up in y direction
  [self move:
   ccp(0, 1)
   ];
}

//=====================MOVING THE TILES=====================

// CGPoint
// https://developer.apple.com/library/mac/documentation//GraphicsImaging/Reference/CGGeometry/index.html#//apple_ref/c/tdef/CGPoint

- (void) move:(CGPoint) moveDir {
  //bottom left
  NSInteger currX = 0;
  NSInteger currY = 0;

  BOOL tilesMovedThisRound = NO;

  // apply direction until the tile hits an edge
  bool validIndex = [self IsValidIndex: currX
                                     y: currY];
  while (validIndex) {
    CGFloat newX = currX + moveDir.x;
    CGFloat newY = currY + moveDir.y;

    if ([self IsValidIndex:newX
                         y:newY]) {
      currX = newX;
      currY = newY;
    } else {
      break;
    }
  }

  // inital row val
  NSInteger initY = currY;

  // opposite value of changing x and y
  NSInteger xOpp = - moveDir.x;
  NSInteger yOpp = - moveDir.y;

  if (xOpp == 0) {
    xOpp = 1;
  }

  if (yOpp == 0) {
    yOpp = 1;
  }

  // all columnds
  while ([self IsValidIndex: currX
                          y: currY]) {
    while ([self IsValidIndex: currX
                            y: currY]) {
      Tile *tile = gridArr[currX][currY];

      if ([tile isEqual:nullTile]) {
        // skip if no tile
        currY += yOpp;
        continue;
      }

      // for new tile location
      NSInteger newX = currX;
      NSInteger newY = currY;

      // keep going until edge or filled tile
      while ([self indexValidAndOpen: newX + moveDir.x
                                         y: newY + moveDir.y]) {
        newX += moveDir.x;
        newY += moveDir.y;
      }

      BOOL makeMove = NO;

      // the cell is filled; check for merging
      if ([self IsValidIndex:newX + moveDir.x
                           y:newY + moveDir.y]) {
        // other tile to check if it should be merged
        NSInteger oTileX = newX + moveDir.x;
        NSInteger oTileY = newY + moveDir.y;
        Tile *oTile = gridArr[oTileX][oTileY];

        // compare value and check already merged
        if (tile.value == oTile.value && !oTile.wasMergedThisRound) {
          // merge tiles together
          [self mergeTileAtLocation:currX
                               y:currY
                 withTileAtIndex:oTileX y:oTileY];
          tilesMovedThisRound = YES;
        } else {
          // can't merge so make move
          makeMove = YES;
        }
      } else {
        // can't merge so make move
        makeMove = YES;
      }

      if (makeMove) {
        // move tile to farthest possible spot
        if (newX != currX || newY !=currY) {
          // move only ifchange
          [self moveTile:tile
                fromIndex:currX
                     oldY:currY
                newX:newX
                newY:newY];
          tilesMovedThisRound = YES;
        }
      }

      // move along the col
      currY += yOpp;
    }
    // move to next col starting at original row
    currX += xOpp;
    currY = initY;
  }

  if (tilesMovedThisRound) {
    [self newRound];
  }
}

- (void)mergeTileAtLocation:(NSInteger)x y:(NSInteger)y withTileAtIndex:(NSInteger)xOtherTile y:(NSInteger)yOtherTile {
  Tile *mergedTile = gridArr[x][y];
  Tile *oTile = gridArr[xOtherTile][yOtherTile];
  self.score += mergedTile.value + oTile.value;
  oTile.value *= 2;

  oTile.wasMergedThisRound = YES;

  if (oTile.value == WIN_VAL) {
    [self winGame];
  }

  gridArr[x][y] = nullTile;

  CGPoint otherTilePosition = [self getCol:xOtherTile row:yOtherTile];
  CCActionMoveTo *moveTo = [CCActionMoveTo actionWithDuration:0.2f position:otherTilePosition];
  CCActionRemove *remove = [CCActionRemove action];

  CCActionCallBlock *mergeTile = [CCActionCallBlock actionWithBlock:^{
    [oTile updateValue];
  }];

  CCActionSequence *sequence = [CCActionSequence actionWithArray:@[moveTo, mergeTile, remove]];
  [mergedTile runAction:sequence];
}

- (void)moveTile:(Tile *)tile fromIndex:(NSInteger)oldX oldY:(NSInteger)oldY newX:(NSInteger)newX newY:(NSInteger)newY {
  gridArr[newX][newY] = gridArr[oldX][oldY];
  gridArr[oldX][oldY] = nullTile;

  CGPoint newPosition = [self getCol:newX row:newY];
  CCActionMoveTo *moveTo = [CCActionMoveTo actionWithDuration:0.2f position:newPosition];
  [tile runAction:moveTo];
}

//=====================UTILITIES=====================

- (BOOL)IsValidIndex:(NSInteger)x y:(NSInteger)y {
  BOOL IsValidIndex = YES;
  IsValidIndex = ((IsValidIndex & (x >= 0)) & (y >= 0));

  if (IsValidIndex) {
    // https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSArray_Class/#//apple_ref/occ/instp/NSArray/count
    IsValidIndex &= x < (int) [gridArr count];
    if (IsValidIndex) {
      IsValidIndex &= y < (int) [(NSMutableArray *) gridArr[x] count];
    }
  }
  return IsValidIndex;
}

- (BOOL) indexValidAndOpen:(NSInteger)x y:(NSInteger)y {
  BOOL IsValidIndex = [self IsValidIndex:x y:y];

  if (!IsValidIndex) {
    return NO;
  }

  BOOL open = [gridArr[x][y] isEqual:nullTile];

  return open;
}

- (void)drawGrid {
  // load single tile
  CCNode *tile = [CCBReader load:@"Tile"];
  colWidth = tile.contentSize.width;
  colHeight = tile.contentSize.height;

  // margin is tile sizes minus grid
  tileMarginY = (self.contentSize.width -
                 (GRID_SZ * colWidth)) / (GRID_SZ + 1);
  tileMarginX = (self.contentSize.height -
                 (GRID_SZ * colHeight)) / (GRID_SZ + 1);
  
  // init x and y
  float x = tileMarginX;
  float y = tileMarginY;

  for (int i = 0; i < GRID_SZ; i++) {
    // each row
    x = tileMarginX;

    for (int j = 0; j < GRID_SZ; j++) {
      // each col in the row
      CCNodeColor *backgroundTile = [CCNodeColor nodeWithColor:[CCColor grayColor]];
      backgroundTile.contentSize = CGSizeMake(colWidth, colHeight);
      backgroundTile.position = ccp(x, y);
      [self addChild:backgroundTile];

      x += colWidth + tileMarginX;
    }
    y += colHeight + tileMarginY;
  }
}

//=====================UTILITIES FOR TILES=====================

- (id) getTileAtIndex:(NSInteger)x y:(NSInteger)y {
  if (![self IsValidIndex:x y:y]) {
    return nullTile;
  } else {
    return gridArr[x][y];
  }
}
- (NSArray *) getNeighbors: (int) xVal y:(int) yVal{
    Tile *topNeighbor = [self getTileAtIndex:xVal y: yVal + 1];
    Tile *bottomNeighbor = [self getTileAtIndex:xVal y: yVal - 1];
    Tile *leftNeighbor = [self getTileAtIndex: xVal - 1 y: yVal];
    Tile *rightNeighbor = [self getTileAtIndex: xVal + 1 y: yVal];
    
    return @[topNeighbor, bottomNeighbor, leftNeighbor, rightNeighbor];
}

- (void)createStartTiles {
  for (int i = 0; i < NUM_START; i++) {
    [self createRandTile];
  }
}

- (void)createRandTile {
  BOOL created = NO;

  while (!created) {
    NSInteger randomRow = arc4random() % GRID_SZ;
    NSInteger randomColumn = arc4random() % GRID_SZ;

    BOOL positionFree = (gridArr[randomColumn][randomRow] == nullTile);

    if (positionFree) {
      [self addTileAtCol:randomColumn row:randomRow];
      created = YES;
    }
  }
}

- (void) addTileAtCol:(NSInteger) col row:(NSInteger)row {
  Tile *tile = (Tile *) [CCBReader load:@"Tile"];
  gridArr[col][row] = tile;
  tile.scale = 0.f;
  [self addChild:tile];
  tile.position = [self getCol:col row:row];
   
  // tile spawn animation
  // http://cocos2d.spritebuilder.com/docs/api/Classes/CCActionDelay.html
  CCActionDelay *delay = [CCActionDelay actionWithDuration:0.3f];
  CCActionScaleTo *scaleUp = [CCActionScaleTo actionWithDuration:0.2f scale:1.f];
  CCActionSequence *sequence = [CCActionSequence actionWithArray:@[delay, scaleUp]];
  [tile runAction:sequence];
}

- (CGPoint) getCol:(NSInteger)col row:(NSInteger)row {
  // https://developer.apple.com/library/mac/documentation//GraphicsImaging/Reference/CGGeometry/index.html
  // http://stackoverflow.com/questions/1746378/cgpointmake-explaination-needed
  NSInteger x = tileMarginY + col * (tileMarginY + colWidth);
  NSInteger y = tileMarginX + row * (tileMarginX + colHeight);

  return CGPointMake(x,y);
}

@end
