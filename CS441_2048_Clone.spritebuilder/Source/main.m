//
//  main.m
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/3/16.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
  @autoreleasepool {
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    return retVal;
  }
}

