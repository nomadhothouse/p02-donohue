//
//  GameEnd.m
//  CS441_2048_Clone
//
//  Created by Thomas Donohue on 2/5/16.
//

#import "GameEnd.h"

@implementation GameEnd {
    // the message in the modal i.e "You win!"
    CCLabelTTF *_messageLabel;
    // the score the player has after the game is over
    CCLabelTTF *_scoreLabel;
}

// change message in modal
- (void)setMsg:(NSString *) msg score:(NSInteger)score {
    _messageLabel.string = msg;
    _scoreLabel.string = [NSString stringWithFormat:@"%ld", (long)score];
}


// restart the main screen for new game
- (void)newGame {
    // http://cocos2d.spritebuilder.com/docs/api/Classes/CCScene.html
    CCScene *mainScreen = [CCBReader
                           loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector]
     replaceScene: mainScreen];
}


@end